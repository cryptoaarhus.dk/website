const eventDates = [
  ['2025-02-08T13:00', '2025-02-08T17:00'],
  ['2025-03-08T13:00', '2025-03-08T17:00'],
  ['2025-04-12T13:00', '2025-04-12T17:00'],
  ['2025-05-10T13:00', '2025-05-10T17:00'],
  ['2025-06-14T13:00', '2025-06-14T17:00'],
  ['2025-08-09T13:00', '2025-08-09T17:00'],
  ['2025-09-06T13:00', '2025-09-06T17:00'],
  ['2025-10-11T13:00', '2025-10-11T17:00'],
  ['2025-11-08T13:00', '2025-11-08T17:00'],
  ['2025-12-06T13:00', '2025-12-06T17:00']
];

const language = document.getElementsByTagName('html')[0].getAttribute('lang');
const weekdaysDanish = ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'];
const weekdaysEnglish= ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const monthsDanish= [
  'januar',
  'februar',
  'marts',
  'april',
  'maj',
  'juni',
  'juli',
  'august',
  'september',
  'oktober',
  'november',
  'december',
];
const monthsEnglish= [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

class EventDate {
  constructor(start, end) {
    this.start = new Date(start);
    this.end = new Date(end);
  }

  timeWindow(now) {
    let unixTimestamp = now.getTime();
    if(unixTimestamp < this.start.getTime()){
      return 'future';
    } else if (unixTimestamp > this.end.getTime()) {
      return 'past';
    } else {
      return 'present';
    }
  }
}

function osmMap() {
  let lat = 56.158432;
  let lon = 10.206393;
  let zoom = 18;
  let position = ol.proj.fromLonLat([lon, lat]);
  let positionFeature = new ol.Feature({
    type: 'geoMarker',
    geometry: new ol.geom.Point(position)
  });

  new ol.Map({
    target: 'Map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      }),
      new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [positionFeature]
        }),
        style: new ol.style.Style({
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: 'rgba(255,0,0,0.8)'
            }),
            radius: 10
          })
        })
      })
    ],
    view: new ol.View({
      center: position,
      zoom: zoom
    }),
    interactions: [], // No dragging or other funny biz
  });
}

/** take the day-part from a Date object and render it as a string */
function fmtDay(dateTime) {
  if(language == 'da') {
    return (
      weekdaysDanish[dateTime.getDay()]
      + ' d. '
      + dateTime.getDate().toString()
      + '. '
      + monthsDanish[dateTime.getMonth()]
    );
  } else { // english
    let day = dateTime.getDate();
    let suffix = 'th';
    if(day % 10 == 1){
      if(day != 11 ) { suffix = 'st'; }
    } else if(day % 10 == 2) {
      if(day != 12) { suffix = 'nd'; }
    } else if(day % 10 == 3){
      if(day != 13) { suffix = 'rd'; }
    }
    return (
      weekdaysEnglish[dateTime.getDay()]
      + ', '
      + monthsEnglish[dateTime.getMonth()]
      + ' '
      + day + suffix
    );
    
  }
}

/** take the time-part from a Date object and render it as a string */
function fmtTime(dateTime) {
  let hours = dateTime.getHours();
  let minutes = dateTime.getMinutes();
  
  return hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0');
}

/** Render events inside a HTML container */
function showEvents(container) {
  let now = new Date();
  let divs = eventDates.map(([start, end]) => {
    let evd = new EventDate(start, end);
    let eventDiv = document.getElementById('event-tpl').content.firstElementChild.cloneNode(true);
    eventDiv.querySelector('.card-title').innerText = fmtDay(evd.start);
    eventDiv.querySelector('.start-time').innerText = fmtTime(evd.start);
    eventDiv.querySelector('.end-time').innerText = fmtTime(evd.end);
    eventDiv.classList.add('event', evd.timeWindow(now));
    return eventDiv;
  });
  container.replaceChildren(...divs);
}

function initMainPage() {
  osmMap();
  showEvents(document.getElementById('next-times'));
}
