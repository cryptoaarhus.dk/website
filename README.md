# cryptoaarhus.dk

[![Build Status](https://drone.data.coop/api/badges/cryptoaarhus.dk/website/status.svg)](https://drone.data.coop/cryptoaarhus.dk/website)

This repo holds the homepage of https://cryptoaarhus.dk/

## Running locally for development:

It's just a static web site.

## Publishing to cryptoaarhus.dk

If you have push access to `master` at https://git.data.coop/cryptoaarhus.dk/website, doing so will trigger the (re)creation of an nginx container that serves the website.
